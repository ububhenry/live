package http_utils

import (
	"bufio"
	"crypto/aes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"live/data"
	"live/msg"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

var Token = ""
var DEFAULT_KEY = "qwertyui12345678"

func RequestApi() {
	fmt.Println("RequestApi")
}

func GetLiveList(liveType string, isSave bool) ([]data.VData, error) {
	if Token == "" {
		initToken()
	}
	var tokenAvai = CheckToken()
	if !tokenAvai {
		return []data.VData{}, errors.New("Token 过期")
	}

	var (
		err     error
		pageNum = 1
		size    = "10"
		resp    *resty.Response
		tmps    []data.VData
	)
	for {
		var cli = resty.New()
		resp, err = cli.R().SetHeaders(map[string]string{
			"User-Agent":     "okhttp/3.14.9",
			"Host":           "al.ailangb.com",
			"token":          Token,
			"time":           fmt.Sprintf("%d", time.Now().UnixNano()/1e6),
			"version":        "3.11.4.1",
			"frond-host":     "https://al.ailangb.com/live-ns/index/getVideoList",
			"Content-Length": "69",
			"dev":            "2",
			"Content-Type":   "application/json;charset=UTF-8",
		}).SetBody(map[string]interface{}{
			"cate":      liveType,
			"condition": "",
			"page_num":  pageNum,
			"page_size": size,
		}).Post("https://al.ailangb.com/live-ns/index/getVideoList")
		if err != nil {
			msg.SendMessage(err.Error())

			log.Println()
		}
		var res data.VideoList
		err = json.Unmarshal(resp.Body(), &res)
		if err != nil {
			log.Println("Unmarshal err: ", err)
		}
		if strings.Contains(string(resp.Body()), "会话过期") {
			msg.SendMessage("会话过期 重新获取 token!")
		}
		var filter []data.VData
		for _, item := range res.Data.Data {
			if strings.Contains(item.Title, "银行卡") || strings.Contains(item.Title, "快三") || strings.Contains(item.Title, "六合彩") || strings.Contains(item.Title, "充值") || strings.Contains(item.Title, "带你飞") {
				continue
			}
			if strings.Contains(item.HostName, "快三") || strings.Contains(item.HostName, "带飞") {
				continue
			}
			item.NplayFlv = DecodeLiveUrl(item.NplayFlv)
			filter = append(filter, item)
		}
		res.Data.Data = filter
		tmps = append(tmps, res.Data.Data...)
		pageNum++

		if isSave {
			lives := WrapLive(res.Data.Data) //列表数据和详情数据合并
			err := data.InsertLives(lives)
			if err != nil {
				return tmps, err
			}
			fmt.Println("save data success  page: ", pageNum)
		} else {
			fmt.Println("-------页码---", pageNum)
		}

		if !res.Data.HasNext {
			break
		}
		time.Sleep(time.Millisecond * 500)
	}
	return tmps, nil
}
func WrapLive(datas []data.VData) []data.Live {
	var res []data.Live
	for _, item := range datas {
		info, _ := GetLiveInfo(strconv.Itoa(item.Id))
		live := data.Live{FirstData: item, SecondData: info}
		res = append(res, live)
	}
	return res
}
func GetLiveInfo(lid string) (data.LiveInfo, error) {
	var res data.LiveInfo

	if Token == "" {
		initToken()
	}

	var tokenAvai = CheckToken()
	if !tokenAvai {
		// GetToken()
		return res, errors.New("Token 过期")
	}
	var (
		err  error
		resp *resty.Response
	)
	var cli = resty.New()
	resp, err = cli.R().SetHeaders(map[string]string{
		"User-Agent":     "okhttp/3.14.9",
		"Host":           "al.ailangb.com",
		"token":          Token,
		"time":           fmt.Sprintf("%d", time.Now().UnixNano()/1e6),
		"version":        "3.11.4.1",
		"frond-host":     "https://al.ailangb.com/live-ns/index/getVideoList",
		"Content-Length": "69",
		"dev":            "2",
		"Content-Type":   "application/json;charset=UTF-8",
	}).SetBody(map[string]interface{}{
		"room_id": lid,
	}).Post("https://al.ailangb.com/live-ns/index/user_video_async")
	if err != nil {
		msg.SendMessage(err.Error())
	}
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		msg.SendMessage(err.Error())
	}
	if strings.Contains(string(resp.Body()), "会话过期") {
		msg.SendMessage("会话过期 重新获取 token!")
	}
	res.Data.NplayFlv = DecodeLiveUrl(res.Data.NplayFlv)
	return res, err
}

func initToken() {
	file := "./token.txt"
	f, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	chunks := make([]byte, 0)
	buf := make([]byte, 2048)
	for {
		n, err := f.Read(buf)
		if err != nil && err != io.EOF {
			panic(err)
		}
		if 0 == n {
			break
		}
		chunks = append(chunks, buf[:n]...)
	}
 	TokenTEMP:= string(chunks)
 	Token=strings.Replace(TokenTEMP,"\n","",-1)
 }

func CheckToken() bool {
	var (
		cli  = resty.New()
		err  error
		resp *resty.Response
	)
	resp, err = cli.R().SetHeaders(map[string]string{
		"User-Agent":     "okhttp/3.14.9",
		"Host":           "al.ailangb.com",
		"token":          Token,
		"time":           fmt.Sprintf("%d", time.Now().UnixNano()/1e6),
		"version":        "3.11.4.1",
		"frond-host":     "https://al.ailangb.com/live-ns/index/getVideoList",
		"Content-Length": "69",
		"dev":            "2",
		"Content-Type":   "application/json;charset=UTF-8",
	}).SetBody(map[string]interface{}{
		"cate":      "1",
		"condition": "",
		"page_num":  1,
		"page_size": "10",
	}).Post("https://al.ailangb.com/live-ns/index/getVideoList")
	if err != nil {
		log.Println("request error: ", err)
		return false
	}
	if strings.Contains(string(resp.Body()), "会话过期") || strings.Contains(string(resp.Body()), "网络波动") {
		log.Println("返回信息异常，内容： ", string(resp.Body()))
		log.Println("请尝试重新获取 token!")
		GetToken()
		return true
	}
	return true
}

func GetToken() {
	initToken()
	return
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("开始获取手机号:")
	fmt.Println("----------------")
	fmt.Println("请输入手机号:")
	phone, _ := reader.ReadString('\n')
	phone = strings.Replace(phone, "\n", "", -1)
	var resp *resty.Response
	var err error
	if phone != "" {
		cli := resty.New()
		resp, err = cli.R().SetHeaders(map[string]string{
			"User-Agent":   "okhttp/3.14.9",
			"version":      "3.11.4.1",
			"dev":          "2",
			"Content-Type": "application/json",
		}).SetBody(map[string]interface{}{"phone": phone}).Post("https://al.ailangb.com/live-ns/phone/plat-verify")
		if err != nil {
			log.Print(err)
		}
		fmt.Println(string(resp.Body()))
		fmt.Println("验证码已经发送，请输入验证码获取 token\n")
		code, _ := reader.ReadString('\n')
		code = strings.Replace(code, "\n", "", -1)
		if code != "" {
			ip, _ := GetOutBoundIp()
			resp, err = cli.R().SetHeaders(map[string]string{
				"User-Agent":     "okhttp/3.14.9",
				"Host":           "al.ailangb.com",
				"time":           fmt.Sprintf("%d", time.Now().UnixNano()/1e6),
				"version":        "3.11.4.1",
				"frond-host":     "https://al.ailangb.com/platform-ns/v1.0/login-phone",
				"Content-Length": "199",
				"token":          "",
				"dev":            "2",
				"Content-Type":   "application/json;charset=UTF-8",
			}).SetBody(map[string]interface{}{
				"agent":        "7702",
				"code":         code,
				"deviceId":     "3BE2D98B9C904BA26AD9EAA82553E11E4BF4D97B",
				"inviter_code": "",
				"ip":           ip,
				"login_dev":    2,
				"mobile":       phone,
				"phoneModel":   "SM-G9910",
				"token":        "",
			}).Post("https://al.ailangb.com/platform-ns/v1.0/login-phone")
			if err != nil {
				log.Print(err)
			}
			var lgRs data.LogResData
			err = json.Unmarshal(resp.Body(), &lgRs)
			if err != nil {
				log.Print("login error, err:=", err)
			}
			fmt.Printf("当前登录 token: ", lgRs.Data.Token)
			Token = lgRs.Data.Token
			log.Println("更新 token 文件")
			UpdateTokenFile()
			log.Println("token 文件更新完成")
		}
	}
}

func ParseLiveUrls(list data.VideoList) data.VideoList {
	for _, item := range list.Data.Data {
		FlyAdd := DecodeLiveUrl(item.NplayFlv)
		item.NplayFlv = FlyAdd
		// fmt.Printf("userId=%s, title=%s,liveUrl=%s ,createType=%t \n", item.UserId, item.Title, FlyAdd, item.CreateType)
	}
	return list
}

func DecodeLiveUrl(url string) string {
	uB, err := base64.StdEncoding.DecodeString(url)
	if err != nil {
		log.Panic(err)
	}
	res := AesDecryptECB(uB, []byte(DEFAULT_KEY))
	return string(res)
}

func AesDecryptECB(encrypted []byte, key []byte) (decrypted []byte) {
	cipher, _ := aes.NewCipher(generateKey(key))
	decrypted = make([]byte, len(encrypted))
	//
	for bs, be := 0, cipher.BlockSize(); bs < len(encrypted); bs, be = bs+cipher.BlockSize(), be+cipher.BlockSize() {
		cipher.Decrypt(decrypted[bs:be], encrypted[bs:be])
	}

	trim := 0
	if len(decrypted) > 0 {
		trim = len(decrypted) - int(decrypted[len(decrypted)-1])
	}

	return decrypted[:trim]
}

func generateKey(key []byte) (genKey []byte) {
	genKey = make([]byte, 16)
	copy(genKey, key)
	for i := 16; i < len(key); {
		for j := 0; j < 16 && i < len(key); j, i = j+1, i+1 {
			genKey[j] ^= key[i]
		}
	}
	return genKey
}

func GetOutBoundIp() (string, error) {
	conn, err := net.Dial("udp", "8.8.8.8:53")
	if err != nil {
		fmt.Println(err)
		return "", nil
	}
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	fmt.Println(localAddr.String())
	ip := strings.Split(localAddr.String(), ";")[0]
	return ip, nil
}

func UpdateTokenFile() {
	err := ioutil.WriteFile("token.txt", []byte(Token), 0644)
	if err != nil {
		panic(err)
	}
}

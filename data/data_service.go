package data

import (
	"database/sql"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"

	"fmt"
	"log"
)

var (
	Dbs [8]*sql.DB
	err error
)

func init() {
	dsnJili := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "U7f16Y3v6HjA1fLj", "172.19.0.141", 3306, "db_yy", "utf8mb4")
	dsn51kc := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "avbhpyU!YyJ2swqao4w", "172.19.0.63", 3306, "db_yy", "utf8mb4")
	dsn91Lu := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "C7x9i0RhmqRPCk6swXQH", "172.19.0.7", 3306, "db_yy", "utf8mb4")
	dsn51Lu := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "2JWhfowVoarzZNAj", "172.19.0.25", 3306, "db_yy", "utf8mb4")
	dsnLubei := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "wvqhthpbd!Wv$da4", "172.19.0.12", 3306, "db_yy", "utf8mb4")
	dsnXc51 := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "PK*lxzBZ2N9QkXsc", "172.19.0.70", 3306, "db_yy", "utf8mb4")
	dsnCcav := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "*mvf3q7z9GcJ7Yha", "172.19.0.50", 3306, "db_yy", "utf8mb4")
	dsnSjsp := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s", "root", "W*I&rI^77uzNda!K", "172.19.0.23", 3306, "db_yy", "utf8mb4") //sejiao

	Dbs[0], err = sql.Open("mysql", dsnJili)
	Dbs[1], err = sql.Open("mysql", dsn51kc)
	Dbs[2], err = sql.Open("mysql", dsn91Lu)
	Dbs[3], err = sql.Open("mysql", dsn51Lu)
	Dbs[4], err = sql.Open("mysql", dsnLubei)
	Dbs[5], err = sql.Open("mysql", dsnXc51)
	Dbs[6], err = sql.Open("mysql", dsnCcav)
	Dbs[7], err = sql.Open("mysql", dsnSjsp)

	if err != nil {
		log.Fatalln("连接mysql失败: ", err)
	}

}

func CreateTable() {
	sql := `
	 create table IF NOT EXISTS t_streamer(
			id bigint unsigned not null auto_increment,
			title 		varchar(100) NOT NULL DEFAULT '' COMMENT 'title',
			user_id 	varchar(50) NOT NULL DEFAULT '' COMMENT 'UserId', 
			live_image  varchar(100) NOT NULL DEFAULT '' COMMENT 'liveImage', 
			live_in  	int NOT NULL DEFAULT '0' COMMENT 'LiveIn', 
			is_live_pay  int NOT NULL DEFAULT '0' COMMENT 'IsLivePay', 
			watch_number  int NOT NULL DEFAULT '100' COMMENT 'watch_number', 
			live_fee  int NOT NULL DEFAULT '0' COMMENT 'live_fee', 
			host_name  varchar(50) NOT NULL DEFAULT '' COMMENT 'HostName', 
			nplay_flv  varchar(1000) NOT NULL DEFAULT '' COMMENT 'nplay_flv', 

			head_image  varchar(100) NOT NULL DEFAULT '' COMMENT 'head_image', 
			is_pay_over int NOT NULL DEFAULT '0' COMMENT 'is_pay_over', 
			nickName  varchar(50) NOT NULL DEFAULT '' COMMENT 'nickName', 
			online_status boolean  default false COMMENT 'online_status',
			ut TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on UPDATE CURRENT_TIMESTAMP ,
			primary key (id),UNIQUE (user_id)
	 ) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4;`
	for index, db := range Dbs {
		_, err := db.Exec(sql)
		if err != nil {
			log.Println("创建表失败: ", index, " 错误： ", err)
		}
	}

}

// "zhangsan"+, 18+i
func InsertLives(datas []Live) error {
	for _, data := range datas {
		for index, db := range Dbs {
			_, err := InsertLive(data, db)
			if err != nil {
				fmt.Println("InsertLive : ", index, " error: ", err)
				return err
			}
		}
	}
	return nil
}
func InsertLive(data Live, db *sql.DB) (string, error) {
	sid := strconv.Itoa(data.FirstData.Id)
	item := data.FirstData
	info := data.SecondData.Data
	rows, err := db.Query("select user_id,nplay_flv from t_streamer where user_id=?", item.UserId)
	if err != nil {
		return sid, err
	}
	defer rows.Close()

	if rows.Next() {
		_, err := db.Exec("update t_streamer set title=?,live_image=?,live_in=?,is_live_pay=?,live_fee=?,nplay_flv=?,head_image=?,is_pay_over=?,nickName=?,online_status=? where user_id=?", item.Title, item.LiveImage, item.LiveIn, item.IsLivePay, item.LiveFee, item.NplayFlv, info.HeadImage, info.IsPayOver, info.NickName, info.OnlineStatus, info.RoomId)
		if err != nil {
			return sid, err
		}

		fmt.Println("userId=", item.UserId, " 更新完成")

	} else {
		_, err := db.Exec("insert into t_streamer (title, user_id,live_image,live_in,is_live_pay,live_fee,host_name,nplay_flv,head_image,is_pay_over,nickName,online_status) value (?,?,?,?,?,?,?,?,?,?,?,?)", item.Title, item.UserId, item.LiveImage, item.LiveIn, item.IsLivePay, item.LiveFee, item.HostName, item.NplayFlv, info.HeadImage, info.IsPayOver, info.NickName, info.OnlineStatus)
		if err != nil {
			return sid, err
		}
		fmt.Println("userId=", item.UserId, " 添加完成")

	}
	return sid, nil
	// fmt.Printf("userId=%s, title=%s,liveUrl=%s ,createType=%t \n", item.UserId, item.Title, FlyAdd, item.CreateType)
}

func SetOnlineStatue(ids []string) error {
	idsStr := strings.Join(ids, ",")
	updateSql := fmt.Sprintf("update t_streamer set online_status=0 where user_id not in (%1s)", idsStr)
	for index, db := range Dbs {
		_, err := db.Exec(updateSql)
		if err != nil {
			fmt.Println("SetOnlineStatue : ", index, " error: ", err)
			return err
		}
	}
	return nil

}

type VideoList struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		Data    []VData `json:"data"`
		Count   int     `json:"count"`
		HasNext bool    `json:"has_next"`
		CurPage int     `json:"cur_page"`
	} `json:"data"`
}
type Live struct {
	FirstData  VData
	SecondData LiveInfo
}
type VData struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	GroupId     string `json:"groupId"`
	GroupId1    string `json:"group_id"`
	UserId      string `json:"userId"`
	LiveImage   string `json:"liveImage"`
	LiveIn      string `json:"liveIn"`
	IsLivePay   string `json:"isLivePay"`
	WatchNumber string `json:"watchNumber"`
	LiveFee     string `json:"liveFee"`
	LotteryId   string `json:"lotteryId"`
	HostName    string `json:"hostName"`
	LotteryName string `json:"lotteryName"`
	CreateType  bool   `json:"createType"`
	NplayFlv    string `json:"nplayFlv"`
}

type LogResData struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		Token             string      `json:"token"`
		Id                string      `json:"id"`
		UserName          string      `json:"user_name"`
		NickName          string      `json:"nick_name"`
		Vip               int         `json:"vip"`
		Heag              string      `json:"heag"`
		TotalAccount      float64     `json:"total_account"`
		LevelIntegral     float64     `json:"level_integral"`
		CodeAccount       float64     `json:"code_account"`
		CodeTotal         float64     `json:"code_total"`
		Status            int         `json:"status"`
		InviterCode       string      `json:"inviter_code"`
		Speak             interface{} `json:"speak"`
		NextLevelIntegral int         `json:"next_level_integral"`
		Wechat            string      `json:"wechat"`
		ChannelCode       string      `json:"channelCode"`
		Pid               int         `json:"pid"`
	} `json:"data"`
}

type LiveInfo struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		RoomId       int     `json:"room_id"`
		HeadImage    string  `json:"head_image"`
		OnlineStatus bool    `json:"online_status"`
		LiveIn       int     `json:"live_in"`
		VoteNumber   float64 `json:"vote_number"`
		GroupId      string  `json:"group_id"`
		LiveFee      int     `json:"live_fee"`
		Officer      int     `json:"officer"`
		HasFocus     bool    `json:"has_focus"`
		NobleCarName string  `json:"noble_car_name"`
		NobleCarUrl  string  `json:"noble_car_url"`
		IsPayOver    int     `json:"is_pay_over"`
		IsLivePay    int     `json:"is_live_pay"`
		WatchNumber  int     `json:"watch_number"`
		GuardNumber  int     `json:"guard_number"`
		GuardType    int     `json:"guardType"`
		LastDay      int     `json:"lastDay"`
		NickName     string  `json:"nickName"`
		NplayFlv     string  `json:"nplayFlv"`
	}
}

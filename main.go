package main

import (
	"fmt"
	"live/data"
	"live/http_utils"
	"live/msg"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func main2() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.GET("/live/getLiveList", func(c *gin.Context) {
		liveType := c.DefaultQuery("type", "1")
		// pageNumber := c.DefaultQuery("lastname", "1") // shortcut for c.Request.URL.Query().Get("lastname")
		list, _ := http_utils.GetLiveList(liveType, false)

		c.JSON(http.StatusOK, list)
	})
	r.GET("/live/getLiveInfo", func(c *gin.Context) {
		roomId, exist := c.GetQuery("lid")
		if exist {
			info, err := http_utils.GetLiveInfo(roomId)
			if err != nil {
				c.JSON(http.StatusOK, map[string]interface{}{
					"msg": err.Error(),
				})

			} else {
				c.JSON(http.StatusOK, info.Data)

			}

		} else {
			c.JSON(http.StatusOK, map[string]interface{}{
				"msg": "param lid 不存在",
			})
		}
	})
	r.Run(":22222")
}

func main() {
	loadDataFromAiLang()
}
func loadDataFromAiLang() {
	msg.SendMessage("开始创建数据表")
	data.CreateTable()
	msg.SendMessage("数据表创建完成")
	fetchFlag := make(chan int)
	msg.SendMessage("启动协程获取数据")
	for {
		go fetchData(fetchFlag)
		fetRes := <-fetchFlag
		if fetRes == 2 {
			break
		}
		msg.SendMessage("获取数据完成")
		result := rand.Int31n(5) + 2
		time.Sleep(time.Duration(result) * time.Minute)
	}
	msg.SendMessage("脚本中断")

}

func fetchData(c chan int) {
	types := [...]int{1, 4}
	updateId := []string{}
	for _, item := range types {
		fmt.Println("开始获取type=", item, "的数据")
		updateList, err := http_utils.GetLiveList(strconv.Itoa(item), true)
		if err != nil {
			c <- 2
			break
		}
		for _, data := range updateList {
			updateId = append(updateId, strconv.Itoa(data.Id))
		}
	}
	err := data.SetOnlineStatue(updateId)
	if err != nil {
		msg.SendMessage(err.Error())
	}
	c <- 1
}
func GracefullExit() {
	fmt.Println("Start Exit...")
	fmt.Println("Execute Clean...")
	fmt.Println("End Exit...")
	os.Exit(0)
}
